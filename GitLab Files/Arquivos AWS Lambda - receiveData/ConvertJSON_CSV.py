import json
import csv
import io

class ConvertFile:
    def convertjson_csv(self, json_data):
        data = json.loads(json.dumps(json_data))
        headers = data[0].keys()
        arquivo = io.StringIO()
        
        writer = csv.DictWriter(arquivo, fieldnames=headers)
        writer.writeheader()
        writer.writerows(data)
        return arquivo


    def convertmap_csv(self, name, map_obj):
        with open(name, 'w', newline='') as f:
            csv.writer(f, delimiter = ',').writerow(list(dict(map_obj))) 


    def convertjsoncsvlocal(self, json_string, file_name):
        data = json.loads(json.dumps(json_string))
        print(data)
        headers = data[0].keys()

        with open(file_name, 'w') as f:
            writer = csv.DictWriter(f, fieldnames=headers)
            writer.writeheader()
            writer.writerows(data)
