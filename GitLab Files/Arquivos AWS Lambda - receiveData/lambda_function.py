import json
from Receive_Data import ReceiveData

# A integracao entre API Gateway - Lambda - Bucket S3 pode ser realizada via HTTP
# atraves do link: 
# https://pxcqybplcd.execute-api.us-west-2.amazonaws.com/default/receiveErgastData

def lambda_handler(event, context):
    
    receivedata = ReceiveData()
    
    try:
        receivedata.receive_data()
        print("Executado com sucesso")
    except Exception as exception:
        print("Erro: ", exception)
    return {
        'statusCode': 200,
        'body': json.dumps('Lambda - Execucao finalizada com sucesso')
    }
