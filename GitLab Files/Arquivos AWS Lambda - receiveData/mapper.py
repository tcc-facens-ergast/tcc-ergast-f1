
class Mappers:
    # mapper para tratar os campos de races
    def mapper_races(self, content):
        return {'season': content['season'],
                'round': content['round'],
                'url': content['url'],
                'raceName': content['raceName'],
                'circuitId': content['Circuit']['circuitId'],
                'locality': content['Circuit']['Location']['locality'],
                'locality': content['Circuit']['Location']['country'],
                'date': content['date']
                }
        
        
    # mappers para tratar os campos do objeto Driver
    def mapper_results_details_driver(self, content):
        if not isinstance(content, (dict)):
            return {}
        return {
            'name': content['givenName'], 
            'family_name': content['familyName'],
            'birthday': content['dateOfBirth'],
            'nationality': content['nationality']
        }
        
        
    # mappers para tratar os campos do objeto Results Details
    def mapper_results_details(self, content):
        if not isinstance(content, (dict)):
            return {}
        if not isinstance(content['Driver'], (dict, list)):
            return {}
        return {
            'number': content['number'], 
            'position': content['position'],
            'points': content['points'],
            'driver': self.mapper_results_details_driver(content['Driver'])     
        }
        
        
    # mapper para tratar os campos de results
    def mapper_results(self, content):
        return {'season': content['season'],
                'round': content['round'],
                'circuitId': content['Circuit']['circuitId'],
                'locality': content['Circuit']['Location']['locality'],
                'country': content['Circuit']['Location']['country']#,
                #'results': list(map(self.mapper_results_details, content['Results']))
                }


    # mappers para tratar os campos do objeto Driver
    def mapper_qualifying_details_driver(self, content):
        if not isinstance(content, (dict)):
            return {}
        return {
            'name': content['givenName'], 
            'family_name': content['familyName'],
            'birthday': content['dateOfBirth'],
            'nationality': content['nationality']
        }


    # mappers para tratar os campos do objeto Qualifying Details
    def mapper_qualifying_details(self, content):
        if not isinstance(content, (dict)):
            return {}
        if not isinstance(content['Driver'], (dict, list)):
            return {}
        return {
            'number': content['number'], 
            'position': content['position'],
            'driver': self.mapper_qualifying_details_driver(content['Driver'])     
        }


    # mapper para tratar os campos do recurso qualifying
    def mapper_qualifying(self, content):
        return {
            'season': content['season'],
            'round': content['round'],
            'raceName': content['raceName'],
            'circuitId': content['Circuit']['circuitId'],
            'locality': content['Circuit']['Location']['locality'],
            'country': content['Circuit']['Location']['country'],
            'date': content['date']#,
            #'qualifying': list(map(self.mapper_qualifying_details, content['QualifyingResults']))
        }
        
    
    def mapper_laps_timing_vazio(self):
        return {
            'driverId': '', 
            'driverPosition': '',
            'driverTime': ''
        }
        
    
    def mapper_laps_details_vazio(self):
        return {
            'number': '', 
            'timings': ''
        }
        
    
    def mapper_circuits_vazio(self):
        return {
            'circuitId': ''
        }
        
        
    def mapper_circuits_location_vazio(self):
        return {
            'locality': '',
            'country': ''
        }
        
        
    def mapper_laps_vazio(self):
        return {
            'laps': ''
        }
    
        
    # mappers para tratar os campos do objeto Timings
    def mapper_laps_details_timings(self, content):
        if not isinstance(content, (dict)):
            return self.mapper_laps_timing_vazio()
        return {
            'driverId': content['driverId'], 
            'driverPosition': content['position'],
            'driverTime': content['time']
        }
        
        
    # mappers para tratar os campos do objeto Laps
    def mapper_laps_details(self, content):
        if not isinstance(content, (dict)):
            return {}
        if not isinstance(content['Timings'], (dict, list)):
            return self.mapper_laps_details_vazio()
        return {
            'number': content['number'], 
            'timings': list(map(self.mapper_laps_details_timings, content['Timings']))     
        }
        
        
    # mapper para tratar os campos do recurso laps
    def mapper_laps(self, content):
        return {
            'season': content['season'],
            'raceName': content['raceName'],
            'circuitId': content['Circuit']['circuitId'],
            'locality': content['Circuit']['Location']['locality'],
            'country': content['Circuit']['Location']['country'],
            'date': content['date']#,
            #'laps': list(map(self.mapper_laps_details, content['Laps']))
        }


    # mappers para tratar os campos do objeto PitStops    
    def mapper_pitstops_details(self, content):
        if not isinstance(content, (dict)):
            return {}
        return {
            'driverId': content['driverId'], 
            'stop': content['stop'],
            'lap': content['lap'],
            'duration': content['Timings']
        }
        
        
    # mapper para tratar os campos do recurso pitstops
    def mapper_pitstops(self, content):
        return {
            'season': content['season'],
            'raceName': content['raceName'],
            'circuitId': content['Circuit']['circuitId'],
            'locality': content['Circuit']['Location']['locality'],
            'country': content['Circuit']['Location']['country'],
            'date': content['date'],
            'time': content['time']#,
            #'laps': list(map(self.mapper_pitstops_details, content['PitStops']))
        }
