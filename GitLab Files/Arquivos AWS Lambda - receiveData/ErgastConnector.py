import requests

class ErgastConnector:
    def __init__(self, url = None):
        if url != None:
            self.url = url
        else:
            self.url = 'http://ergast.com/api/f1/2023/'

    def get_requests(self, paths = None):
        ergast = self.url + paths + '.json'
        print(ergast)
        res = requests.get(ergast)
        print(res)
        return res.json()
