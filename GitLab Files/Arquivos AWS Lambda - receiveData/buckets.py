import os
from dependencies.boto3 import client
from botocore.exceptions import ClientError

class AmazonS3:

    def __init__(self, AWSAccessKey, AWSSecretKey):
        self.s3 = client('s3', aws_access_key_id = AWSAccessKey, aws_secret_access_key = AWSSecretKey)


    def list_buckets(self):
        res = self.s3.list_buckets()
        for bucket in res['Buckets']:
            print(bucket['Name'])


    def upload_file(self, file_name, bucket, object_name=None):
        if object_name is None:
            object_name = os.path.basename(file_name)

        try:
            response = self.s3.upload_file(file_name, bucket, object_name)
        except ClientError as e:
            return False
        return True
    
    
    def put_file(self, bucket_name, file_name, Data_Object):
        self.s3.put_object(Body=Data_Object, Bucket=bucket_name, Key=file_name)
