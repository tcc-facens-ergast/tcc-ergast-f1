from buckets import AmazonS3
from ErgastConnector import ErgastConnector
from ConvertJSON_CSV import ConvertFile
from mapper import Mappers


class ReceiveData:

    def receive_data(self):
            
        s3 = AmazonS3('AKIAROBE7ACNV2N7LEZH', 'ihRzaEKdZWuUJp2mJBH0BLwgjbAKhNh9jI5QxOoT')
        resources = ['circuits', 'constructors', 'drivers', 'laps', 'pitstops', 'qualifying', 'races', 'results']

        ergast = ErgastConnector()
        mapper = Mappers()
        convert = ConvertFile()
        
        # s3.list_buckets()

        for resource in resources:
            print(resource)
            
            # os recursos laps e pitstops precisam dos parametros temporada (ano) e rodada (round) para
            # que o acesso ao recurso seja bem sucedido. o acesso sera feito de forma fixa para os recursos
            # do round 1
            # adicionalmente, para todos os recursos sera acessado informaçoes da temporada 2023
            if resource == 'laps' or resource == 'pitstops':
                resource = '1/' + resource
                res = ergast.get_requests(resource)
                if resource == '1/laps':
                    resource = 'lap_times'
                else:
                    resource = 'pit_stops'
            else:
                res = ergast.get_requests(resource)
            
            match resource:
                case 'circuits':
                    data_values = res['MRData']['CircuitTable']['Circuits']
                    csv = convert.convertjson_csv(data_values)
                case 'constructors':
                    data_values = res['MRData']['ConstructorTable']['Constructors']
                    csv = convert.convertjson_csv(data_values)
                case 'drivers':
                    data_values = res['MRData']['DriverTable']['Drivers']
                    csv = convert.convertjson_csv(data_values)
                case 'lap_times':
                    data_values = res['MRData']['RaceTable']['Races']
                    data_values_map = list(map(mapper.mapper_laps, data_values))
                    csv = convert.convertjson_csv(data_values_map)
                case 'pitStops':
                    data_values = res['MRData']['RaceTable']['Races']
                    data_values_map = list(map(mapper.mapper_pitstops, data_values))
                    csv = convert.convertjson_csv(data_values_map)
                case 'qualifying':
                    data_values = res['MRData']['RaceTable']['Races']
                    data_values_map = list(map(mapper.mapper_qualifying, data_values))
                    csv = convert.convertjson_csv(data_values_map)
                case 'races':
                    data_values = res['MRData']['RaceTable']['Races']
                    data_values_map = list(map(mapper.mapper_races, data_values))
                    csv = convert.convertjson_csv(data_values_map)
                case 'results':
                    data_values = res['MRData']['RaceTable']['Races']
                    data_values_map = list(map(mapper.mapper_results, data_values))
                    csv = convert.convertjson_csv(data_values_map)
                    
                  
            csv_bytes = str.encode(csv.getvalue())
            folder = 'drop_zone_s3/' + resource + '/' + resource + '.csv'
            
            try:
                s3.put_file('my-drop-zone-bucket', folder, csv_bytes)
            except Exception as exception:
                print("Erro: ", exception)
